<?php
namespace App\Services;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Validator;
use App\Common\Helpers\FileHelper;
/**
 * Created by PhpStorm.
 * User: vuongngo
 * Date: 1/20/19
 * Time: 13:57
 */
class PasswordManager
{
    /**
     * @var string
     */
    private $username = '';
    /**
     * @var string
     */
    private $password = '';

    protected $rules = [
        'password' => [
            'required',
            'min:6',
            'without_spaces',
            'check_letter',
            'check_digit_symbol'
        ]
    ];

    /**
     *
     */
    public function __construct()
    {

    }

    public function validatePassword($password = ''){
        $result = [
            'status' => true,
            'error' => null
        ];
        $validator = Validator::make(['password' => $password], $this->rules);

        if ($validator->fails()) {
            $result['status'] = false;
            $result['error'] = $validator;
        }

        return $result;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function setNewPassword($password = ''){
        $result = $this->validatePassword($password);
        if (!$result['status']){
            return false;
        }
        $password = $this->encrypt($password);
        $this->setPassword($password);

        return true;
    }

    /**
     * @param string $username
     * @return bool
     */
    public function setNewUsername($username = ''){
        $this->setUserName($username);
        return true;
    }

    /**
     * @param string $password
     * @return string
     */
    protected function encrypt($password = ''){
        return encrypt($password);
    }

    /**
     * @param $password
     * @return bool
     */
    protected function verifyPassword($password)
    {
        $verifyPassword = decrypt($this->password);
        if (strcmp($password, $verifyPassword) == 0){
            return true;
        }

        return false;
    }

    /**
     * @param string $username
     * @return $this
     */
    protected function setUserName($username = ''){
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    protected function getUserName(){
        return $this->username;
    }

    /**
     * @param $password
     * @return $this
     */
    protected function setPassword($password){
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    protected function getPassword(){
        return $this->password;
    }

    /**
     * @param string $fileName
     * @return mixed
     */
    public function saveDataToFile($fileName = 'password.txt'){
        $data = [
            'username' => $this->username,
            'password' => $this->password
        ];

        return FileHelper::saveDataToFile($fileName, $data);
    }

    /**
     * @param string $fileName
     * @return array|mixed
     */
    protected function readDataFromFile($fileName = 'password.txt'){
        return FileHelper::readDataFromFile($fileName);
    }
}