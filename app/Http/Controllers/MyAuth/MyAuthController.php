<?php

namespace App\Http\Controllers\MyAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PasswordManager;

class MyAuthController extends Controller
{
    /**
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createNewUser(Request $request){
        $input = $request->all();
        $username = $input['username'];
        $password = $input['password'];
        $passwordManager = new PasswordManager();
        $validate = $passwordManager->validatePassword($password);
        if (!$validate['status']){
            return redirect()->back()
                ->withErrors($validate['error'])
                ->withInput();
        }

        $passwordManager->setNewPassword($password);
        $passwordManager->setNewUsername($username);
        if ($passwordManager->saveDataToFile()){
            return view('success');
        }

        return redirect()->back();
    }
}
