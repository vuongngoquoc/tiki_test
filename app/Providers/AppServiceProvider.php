<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('without_spaces', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^\S*$/u', $value);
        });

        Validator::extend('check_letter', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^.*(?=.*[a-z])(?=.*[A-Z]).*$/', $value);
        });

        Validator::extend('check_digit_symbol', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^.*(?=.*[0-9])(?=.*[!$#%]).*$/', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
