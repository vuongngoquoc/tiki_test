<?php

namespace App\Common\Helpers;

use Illuminate\Support\Facades\Storage;

class FileHelper
{
    /**
     * @param string $fileName
     * @param array $data
     * @return mixed
     */
    public static function saveDataToFile($fileName = '', $data = []){
        $data = json_encode($data);
        return Storage::put('txt/'.$fileName, $data);
    }

    /**
     * @param string $fileName
     * @return array|mixed
     */
    public static function readDataFromFile($fileName = ''){
        $data = [];
        $path = storage_path() . '/app/txt/'.$fileName;
        if (file_exists($path)){
            $data = json_decode(file_get_contents($path), true);
        }
        return $data;
    }
}